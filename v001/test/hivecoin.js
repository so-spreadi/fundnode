var HiveCoin = artifacts.require("Token");

contract('Token', function(accounts){
    it("should put 10,000 HiveCoin in the first account", function(){
        return HiveCoin.deployed().then(function(instance) {
            return instance.balanceOf.call(accounts[0]);
        }).then(function(balance){
            assert.equal(balance.valueOf(), 10000, "10,000 was NOT in the first account");
        });
    });
    it("should send coins correctly", function(){
        var meta;

        var account_one = accounts[0];
        var account_two = accounts[1];

        var account_one_starting_balance;
        var account_one_ending_balance;
        var account_two_starting_balance;
        var account_two_ending_balance;

        var amount = 10;

        return HiveCoin.deployed().then(function(instance){
            meta = instance;
            return meta.balanceOf.call(account_one);
        }).then(function(balance){
            account_one_starting_balance = balance.toNumber();
            return meta.balanceOf.call(account_two);
        }).then(function(balance){
            account_two_starting_balance = balance.toNumber();
            return meta.transfer(account_two, amount, {from: account_one});
        }).then(function(){
            return meta.balanceOf.call(account_one);
        }).then(function(balance){
            account_one_ending_balance = balance.toNumber();
            return meta.balanceOf.call(account_two);
        }).then(function(balance){
            account_two_ending_balance = balance.toNumber();
            assert.equal(account_one_ending_balance, account_one_starting_balance-amount, "Amount was NOT correctly taken from sender");
            assert.equal(account_two_ending_balance, account_two_starting_balance+amount, "Amount was NOT correctly sent to receiver");
        });
    });
});
