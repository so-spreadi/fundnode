pragma solidity 0.4.15;

contract Asset {
    function transferOwnership(address _newOwner) public returns (bool success);
}

contract ERC20 {

    function approve(address _spender, uint _value) external returns (bool success);
    function allowance(address _owner, address _spender) external constant returns (uint remaining);
    
    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(address indexed _owner, address indexed _spender, uint _value);

}

contract ERC223 {
    function balanceOf(address _who) public constant returns (uint256 _remaining);
    function name() external constant returns (string _name);
    function symbol() external constant returns (string _symbol);
    function decimals() external constant returns (uint8 _decimals);
    function totalSupply() external constant returns (uint256 _supply);
    function transfer(address _to, uint256 _value) public returns (bool _success);
    function transfer(address _to, uint256 _value, bytes _data) public returns (bool _success);
    function transfer(address _to, uint256 _value, bytes _data, string _custom_fallback) public returns (bool _success);
    
    event Transfer(address _from, address _to, uint _value, bytes _data);
}

contract Receiver {
    function tokenFallback(address _from, uint _value, bytes _data);
}

contract Crowdfundable {
    function changePrice(uint256 _newPrice) private returns (bool success);
    function initiateSale(address _initiator, uint256 _goal, uint256 _deadline) public returns (bool _isOnSale);
    function buyTokens() external payable returns (bool _success);

    event DonationAcquired(address _donator, uint256 _amount, bool _isDonation);
}

contract Verifiable {
    function recoverAddr(bytes32 _msgHash, uint8 _v, bytes32 _r, bytes32 _s) public returns (address);
    function isSigned(address _addr, bytes32 _msgHash, uint8 _v, bytes32 _r, bytes32 _s) public returns (bool);
}

contract UpdatingRate {
    function rateTicker();
    function __callback(bytes32 myid, string result, bytes proof);
    function update() payable;
}
