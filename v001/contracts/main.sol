pragma solidity 0.4.15;

import "./interfaces.sol";
import "./safeMath.sol";
import "./storage.sol";

contract Token is Storage, Asset, ERC20, ERC223, SafeMath, Receiver, Crowdfundable, Verifiable {
// TODO: 
//       1. look into Zeppelin Crowdsale contract

    function Token(uint256 _initialCapital, uint256 _initialPrice) {
        require(_initialCapital > 0);
        require(_initialPrice > 0);
        owner = msg.sender;
        price = _initialPrice;
        capitalization = _initialCapital;
        accBooks[owner] = capitalization;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function shredContract() onlyOwner {
        selfdestruct(owner);
    }

    function tokenFallback(address _from, uint256 _value, bytes _data) {
        TKN memory tkn;
        tkn.sender = _from;
        tkn.value = _value;
        tkn.data = _data;
        uint32 u = uint32(_data[3]) + (uint32(_data[2]) << 8) + (uint32(_data[1]) << 16) + (uint32(_data[0]) << 24);
        tkn.sig = bytes4(u);
    }

    function() payable {
        bytes memory empty;
        tokenFallback(msg.sender, msg.value, empty);
    }

    function transferOwnership(address _newOwner) public onlyOwner returns (bool success){
        require (_newOwner != address(0x0));
        owner = _newOwner;
        return true;
    }

//
//    ERC operations implementations
//

    function totalSupply() external constant returns (uint256 _supply) {
        return capitalization;
    }

    function balanceOf(address _who) public constant returns (uint256 _remaining) {
        require(_who != 0x0);
        return accBooks[_who];
    }

    function name() external constant returns (string _name) {
        return tokenLabel;
    }

    function symbol() external constant returns (string _symbol) {
        return tokenSymbol;
    }

    function decimals() external constant returns (uint8 _decimals) {
        return tokenDecimals;
    }

    function allowance(address _owner, address _spender) external constant returns (uint256 _remaining) {
        return loans[_owner][_spender];
    }

    function isContract(address _addr) private constant returns (bool is_contract) {
        uint256 length;
        assembly {
              //retrieve the size of the code on target address, this needs assembly
              length := extcodesize(_addr)
          }
        return (length > 0);
    }

    function transferToAddress(address _to, uint256 _value, bytes _data) private {
      accBooks[msg.sender] = safeSub(balanceOf(msg.sender), _value);
      accBooks[_to] = safeAdd(accBooks[_to], _value);
      Transfer(msg.sender, _to, _value, _data);
    }

    function transferToContract(address _to, uint256 _value, bytes _data) private {
      accBooks[msg.sender] = safeSub(balanceOf(msg.sender), _value);
      accBooks[_to] = safeAdd(accBooks[_to], _value);
      Receiver receiver = Receiver(_to);
      receiver.tokenFallback(msg.sender, _value, _data);
      Transfer(msg.sender, _to, _value, _data);
    }

    function transfer(address _to, uint256 _value) public returns (bool _success) {
        require(!isSaleOngoing);
        require(_to != 0x0);
        require(_value > 0);
        require(accBooks[msg.sender] >= _value);
        require(accBooks[_to] + _value > accBooks[_to]);
        bytes memory _empty;
        if(isContract(_to)) {
            transferToContract(_to, _value, _empty);
            return true;
        }
        else {
            transferToAddress(_to, _value, _empty);
            return true;
        }
        revert();
    }

    function transfer(
        address _to,
        uint256 _value,
        bytes _data
    ) public returns (bool _success) {
        require(!isSaleOngoing);
        require(_to != 0x0);
        require(_value > 0);
        require(accBooks[msg.sender] >= _value);
        require(accBooks[_to] + _value > accBooks[_to]);
        if(isContract(_to)) {
            transferToContract(_to, _value, _data);
            return true;
        }
        else {
            transferToAddress(_to, _value, _data);
            return true;
        }
        revert();
    }

    function transfer(
        address _to,
        uint _value,
        bytes _data,
        string _custom_fallback
    ) public returns (bool _success) {
        require(!isSaleOngoing);
        require(_to != 0x0);
        require(_value > 0);
        require(accBooks[msg.sender] >= _value);
        require(accBooks[_to]+_value > accBooks[_to]);
        if(isContract(_to)) {
            accBooks[msg.sender] = safeSub(accBooks[msg.sender], _value);
            accBooks[_to] = safeAdd(accBooks[_to], _value);
            Receiver receiver = Receiver(_to);
            assert(receiver.call.value(0)(bytes4(sha3(_custom_fallback)), msg.sender, _value, _data));
            Transfer(msg.sender, _to, _value, _data);
            return true;
        }
        else {
            transferToAddress(_to, _value, _data);
            return true;
        }
    }

    function approve(address _spender, uint256 _value) external returns (bool _success) {
        require((_value == 0) || (loans[msg.sender][_spender] == 0));
        loans[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

//
//    Sale/ICO operations
//

    function changePrice(uint256 _newPrice) private onlyOwner returns (bool _success) {
        price = _newPrice;
        return true;
    }

    function initiateSale(
        address _initiator,
        uint256 _goal,
        uint256 _deadline
    ) public onlyOwner returns (bool _isOnSale) {
        require (_goal >= 0);
        require (_deadline >= 0);
        if (isSaleOngoing == false) {
            isSaleOngoing = true;
            campaignHistory[campaignCount] = campaign(_initiator, _goal, 0, _deadline);
            return isSaleOngoing;
        }
        else return false;
    }

    function buyTokens() external payable returns (bool _success) {
        require(!isSaleOngoing);
        uint256 amount = msg.value;
        accBooks[msg.sender] += amount;
        campaignHistory[campaignCount].saleGoal += amount;
        transfer(msg.sender, amount/price);
        DonationAcquired(msg.sender, amount, true);
        return true;
    }

//
//    Signature/Verfication related operations
//

    function recoverAddr(
        bytes32 _msgHash,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) public returns (address) {
        return ecrecover(_msgHash, _v, _r, _s);
    }

    function isSigned(
        address _addr,
        bytes32 _msgHash,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) public returns (bool) {
        return ecrecover(_msgHash, _v, _r, _s) == _addr;
    }
}
