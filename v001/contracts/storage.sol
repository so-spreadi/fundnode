;pragma solidity 0.4.15;

contract Storage {
    address public owner;
    uint256 public price;
    uint256 capitalization;
    string public constant tokenLabel = "HIVE";
    string public constant tokenSymbol = "HVTKN";
    uint8 public constant tokenDecimals = 18;
    bool isSaleOngoing = false;
    uint8 campaignCount;
    
    struct campaign {
        address beneficiary;
        uint256 saleGoal;
        uint256 saleMark;
        uint256 saleDeadline;
    }

    struct TKN {
        address sender;
        uint value;
        bytes data;
        bytes4 sig;
    }

    mapping (address => uint256) accBooks;
    mapping (address => mapping (address => uint256)) loans;
    mapping (uint8 => campaign) campaignHistory;
}
