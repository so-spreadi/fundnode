pragma solidity 0.4.15;

import "https://github.com/ConsenSys/MultiSigWallet/blob/master/MultiSigWalletWithDailyLimit.sol";

contract ERC20Basic {
    uint256 public totalTokens;
    function balanceOf(address who) public constant returns (uint256);
    function transfer(address to, uint256 value) public returns (bool);
}

contract ERC20 is ERC20Basic {
    function allowance(address owner, address spender) public constant returns (uint256);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function approve(address spender, uint256 value) public returns (bool);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract ERC223 is ERC20 {
    function totalSupply() constant returns (uint256 _totalSupply);
    function name() constant returns (string _name);
    function symbol() constant returns (bytes32 _symbol);
    function decimals() constant returns (uint8 _decimals);
    function transfer(address _to, uint _value) returns (bool);
    function transfer(address _to, uint _value, bytes _data) returns (bool);
    function tokenFallback(address _from, uint _value, bytes _data);
    event Transfer(address indexed _from, address indexed _to, uint256 indexed _value, bytes _data);
}

library SafeERC20 {
    function safeTransfer(ERC20Basic token, address to, uint256 value) internal {
      assert(token.transfer(to, value));
    }

    function safeTransferFrom(ERC20 token, address from, address to, uint256 value) internal {
      assert(token.transferFrom(from, to, value));
    }

    function safeApprove(ERC20 token, address spender, uint256 value) internal {
      assert(token.approve(spender, value));
    }
}

library Math {
    function max64(uint64 a, uint64 b) internal constant returns (uint64) {
      return a >= b ? a : b;
    }

    function min64(uint64 a, uint64 b) internal constant returns (uint64) {
      return a < b ? a : b;
    }

    function max256(uint256 a, uint256 b) internal constant returns (uint256) {
      return a >= b ? a : b;
    }

    function min256(uint256 a, uint256 b) internal constant returns (uint256) {
      return a < b ? a : b;
    }
}

library SafeMath {
    function mul(uint256 a, uint256 b) internal constant returns (uint256) {
      uint256 c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint256 a, uint256 b) internal constant returns (uint256) {
      uint256 c = a / b;
      return c;
    }

    function sub(uint256 a, uint256 b) internal constant returns (uint256) {
      assert(b <= a);
      return a - b;
    }

    function add(uint256 a, uint256 b) internal constant returns (uint256) {
      uint256 c = a + b;
      assert(c >= a);
      return c;
    }
}

contract BasicToken is ERC20Basic {
    using SafeMath for uint256;
    mapping(address => uint256) balances;

    function balanceOf(address _owner) public constant returns (uint256 balance) {
      return balances[_owner];
    }

}

contract ContractReceiver {
    struct TKN {
       address sender;
       uint value;
       bytes data;
       bytes4 sig;
    }

    function tokenFallback(address _from, uint _value, bytes _data){
     TKN memory tkn;
     tkn.sender = _from;
     tkn.value = _value;
     tkn.data = _data;
     uint32 u = uint32(_data[3]) + (uint32(_data[2]) << 8) + (uint32(_data[1]) << 16) + (uint32(_data[0]) << 24);
     tkn.sig = bytes4(u);
    }

}

contract StandardToken is ERC20, ERC223, BasicToken {
    mapping (address => mapping (address => uint256)) internal allowed;

    function approve(address _spender, uint256 _value) public returns (bool) {
      allowed[msg.sender][_spender] = _value;
      Approval(msg.sender, _spender, _value);
      return true;
    }

    function allowance(address _owner, address _spender) public constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }

    function increaseApproval (address _spender, uint _addedValue) public returns (bool success) {
      allowed[msg.sender][_spender] = allowed[msg.sender][_spender].add(_addedValue);
      Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
      return true;
    }

    function decreaseApproval (address _spender, uint _subtractedValue) public returns (bool success) {
      uint oldValue = allowed[msg.sender][_spender];
      if (_subtractedValue > oldValue) {
        allowed[msg.sender][_spender] = 0;
      } else {
        allowed[msg.sender][_spender] = oldValue.sub(_subtractedValue);
      }
      Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
      return true;
    }

}

contract HiveToken is StandardToken {
    using SafeMath for uint256;
    string public name;
    string public symbol;
    uint8 public decimals;
    uint256 public totalSupply;
    mapping (address => uint256) loanAgreement;
    mapping (address => uint256) currentLoanAmount;

    function HiveToken() {

    }

    function transfer(address _to, uint _value, bytes _data, string _custom_fallback) returns (bool success) {
      if(isContract(_to)) {
          require (balanceOf(msg.sender) >= _value);
          balances[msg.sender] = balanceOf(msg.sender).sub(_value);
          balances[_to] = balanceOf(_to).add(_value);
          ContractReceiver receiver = ContractReceiver(_to);
          assert(receiver.call.value(0)(bytes4(sha3(_custom_fallback)), msg.sender, _value, _data));
          Transfer(msg.sender, _to, _value, _data);
          return true;
      }
      else {
          return transferToAddress(_to, _value, _data);
      }
    }

    function transfer(address _to, uint _value, bytes _data) returns (bool success) {
      if(isContract(_to)) {
          return transferToContract(_to, _value, _data);
      }
      else {
          return transferToAddress(_to, _value, _data);
      }
    }

    function transfer(address _to, uint _value) returns (bool success) {
      bytes memory empty;
      if(isContract(_to)) {
          return transferToContract(_to, _value, empty);
      }
      else {
          return transferToAddress(_to, _value, empty);
      }
    }

    function isContract(address _addr) private returns (bool is_contract) {
        uint length;
        assembly {
              length := extcodesize(_addr)
        }
        return (length>0);
    }

    function transferToAddress(address _to, uint _value, bytes _data) private returns (bool success) {
      require(balanceOf(msg.sender) >= _value);
      balances[msg.sender] = balanceOf(msg.sender).sub(_value);
      balances[_to] = balanceOf(_to).add(_value);
      Transfer(msg.sender, _to, _value, _data);
      return true;
    }

    function transferToContract(address _to, uint _value, bytes _data) private returns (bool success) {
      require(balanceOf(msg.sender) >= _value);
      balances[msg.sender] = balanceOf(msg.sender).sub(_value);
      balances[_to] = balanceOf(_to).add(_value);
      ContractReceiver receiver = ContractReceiver(_to);
      receiver.tokenFallback(msg.sender, _value, _data);
      Transfer(msg.sender, _to, _value, _data);
      return true;
    }

}
