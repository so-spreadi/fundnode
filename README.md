# Hive Token

### Token specification

Version 0.0.1 is a self-developed smart contract.
Version 0.0.2 is a smart contract that incorporates code of OpenZeppelin's Zeppelin Solidity and ConsenSys MultiSig Wallet.
Please refer to [**Wiki**](https://bitbucket.org/so-spreadi/fundnode/wiki/Home) for more information

> Frequently Used 'Smart Contract'-focused Blogs: [Medium](https://medium.com),[Colony](https://blog.colony.io/),[dApps For Beginners](https://dappsforbeginners.wordpress.com/)

## TODO list:
1. edit the wiki to reflect the changes ;
2. add timestamp blocks ;
3. implement multisig functionality ;
